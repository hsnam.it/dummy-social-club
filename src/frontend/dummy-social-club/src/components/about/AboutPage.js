import React from 'react';

const AboutPage = () => {
  return (
    <div>
      <h2>About</h2>
      <p>
        Just a dummy social club for dummies :))
    </p>
    </div>
  )
}

export default AboutPage;