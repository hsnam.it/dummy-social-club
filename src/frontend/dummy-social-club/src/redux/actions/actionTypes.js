export const LOAD_ALL_POSTS = "LOAD_ALL_POSTS";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";
export const LOGOUT = "LOGOUT";
export const CREATE_NEW_POST = "CREATE_NEW_POST";